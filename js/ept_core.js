(function ($, Drupal) {

  /**
   * EPT Core behavior.
   */
  Drupal.behaviors.eptCore = {
    attach: function (context, settings) {
      $.each(drupalSettings['eptCore'], function(block_class, value) {
        if (value['eptCoreParallax'] != undefined) {
          $('.' + block_class).parallax({
            imageSrc: Drupal.checkPlain(value['eptCoreParallax']['mediaUrl'])
          });
        }

        if (value['eptCoreBackgroundRemoteVideo'] != undefined) {
          if (value['eptCoreBackgroundRemoteVideo']['mediaProvider'] == 'YouTube') {
            const $elements =  $(once('youtube-video', '.' + block_class + ' .bg-inner', context));
            $elements.YTPlayer({
              videoURL: Drupal.checkPlain(value['eptCoreBackgroundRemoteVideo']['mediaUrl']),
              containment: '.' + block_class,
              autoPlay: 1,
              showControls: 0,
              mute: 1,
              startAt: 0,
              opacity: 1,
              addRaster: 1,
              quality: 'default',
              loop: 1
            });
          }
        }
      });
    }
  };

})(jQuery, Drupal);
