# Extra Block Types (EPT): Core

EPT modules provide ability to add different blocks in Layout Builder in
few clicks. EPT Core contains helper functions for EPT modules.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ept_core).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ept_core).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- External Documentation
- Maintainers


## Requirements

This module requires the following modules:

- [Field Group](https://www.drupal.org/project/field_group)
- [Media Library Form API Element](https://www.drupal.org/project/media_library_form_element)

EPT Modules use Media module with Media Image type for background images.
Check Media Image type exists before install EPT Core module.


## Recommended modules-

You can install separate block types from this bunch of EPT modules:
- [EPT Accordion / FAQ](https://www.drupal.org/project/ept_accordion)
- [EPT Basic Button](https://www.drupal.org/project/ept_basic_button)
- [EPT Bootstrap Button](https://www.drupal.org/project/ept_bootstrap_button)
- [EPT Call to Action](https://www.drupal.org/project/ept_cta)
- [EPT Carousel](https://www.drupal.org/project/ept_carousel)
- [EPT Counter](https://www.drupal.org/project/ept_counter)
- [EPT Quote](https://www.drupal.org/project/ept_quote)
- [EPT Micromodal](https://www.drupal.org/project/ept_micromodal)
- [EPT Slick Slider](https://www.drupal.org/project/ept_slick_slider)
- [EPT Slideshow](https://www.drupal.org/project/ept_slideshow)
- [EPT Stats](https://www.drupal.org/project/ept_stats)
- [EPT Tabs](https://www.drupal.org/project/ept_tabs)
- [EPT Text](https://www.drupal.org/project/ept_text)
- [EPT Timeline](https://www.drupal.org/project/ept_timeline)
- [EPT Webform](https://www.drupal.org/project/ept_webform)
- [EPT Webform Popup](https://www.drupal.org/project/ept_webform_popup)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

EPT Core has configuration form with Primary/Secondary colors
and Mobile/Tablet/Desktop breakpoints in
Administration » Configuration » Content authoring
» Extra Block Types (EPT) settings

These configs will be applied to other EPT blocks by default.


## Troubleshooting

- If you see the error during EPT modules installation:
  "The EPT Carousel needs to be created "Image" media type.
  (Currently using Media type Image version Not created)"
  Then you need to create Image media type:
  Structure » Media types » Add media type
- If you use Field Layout module,
  it will automatically apply Layout Builder for new EPT block types.
  So you will need to disable Layout Builder for displaying block type fields,
  for example:

   `/admin/structure/block/block-content/manage/ept_cta/display/default`


## FAQ

**Q: Can I use only one EPT block type, for example EPT Slideshow, without other
modules?**

**A:** Yes, sure. EPT block types tend to be standalone modules.
       You can install only needed block types.


## External Documentation

You can also view the EPT documentation at Drupal Book:
https://drupalbook.org/ept


## Maintainers

- Ivan Abramenko - [levmyshkin](https://www.drupal.org/u/levmyshkin)
- Narine Tsaturyan - [Narine_Tsaturyan](https://www.drupal.org/u/narine_tsaturyan)
